package ru.ifmo.ctddev.igushkin.producer;

/**
 * Created by Sergey.
 */

@FunctionalInterface
public interface TaskFactory<X, Y> {
    Task<X, Y> makeTask();
}
