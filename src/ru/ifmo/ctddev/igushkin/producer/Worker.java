package ru.ifmo.ctddev.igushkin.producer;

import java.util.concurrent.*;

/**
 * Created by Sergey.
 */
public class Worker<X, Y> {

    public static final int QUEUE_SIZE = 10;

    private BlockingQueue<TaskHolder<X, Y>> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    private Publisher<X> publisher;

    public Worker(Publisher<X> publisher) {
        this.publisher = publisher;
    }

    public void addTask(Task<X, Y> task, Y argument) {
        try {
            queue.put(new TaskHolder<>(task, argument));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void executeOneTask() {
        try {
            final TaskHolder<X, Y> t = queue.take();
            try {
                t.complete();
                publisher.publish(t.getResult());
            } catch (TaskHolder.AlreadyDoneException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
