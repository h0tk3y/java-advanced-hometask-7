package ru.ifmo.ctddev.igushkin.producer;

/**
 * Created by Sergey.
 */

@FunctionalInterface
public interface ArgumentsProvider<X> {
    X makeArgument();
}
