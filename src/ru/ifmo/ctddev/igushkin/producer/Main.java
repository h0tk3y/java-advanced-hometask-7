package ru.ifmo.ctddev.igushkin.producer;

/**
 * Created by Sergey.
 */
public class Main {

    public static final int SUBJECTS_COUNT = 10;
    public static final int WORKING_THREADS_COUNT = 1;

    public static void main(String[] args) {
        new ExecutionChain().run();
    }
}
