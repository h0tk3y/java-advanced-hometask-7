package ru.ifmo.ctddev.igushkin.producer;

/**
 * Created by Sergey.
 */
public interface Task<X, Y> {
    X run(Y value);
}
