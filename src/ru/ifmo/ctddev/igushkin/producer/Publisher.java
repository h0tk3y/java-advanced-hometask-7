package ru.ifmo.ctddev.igushkin.producer;

import java.util.concurrent.*;

/**
 * Created by Sergey.
 */
public class Publisher<X> {

    public static final int QUEUE_SIZE = 10;

    private ExecutorService executor;
    private BlockingQueue<X> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);

    public Publisher(ExecutorService executor) {this.executor = executor;}

    public void publish(X result) {
        try {
            queue.put(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void publishOneTask() {
        try {
            final X item = queue.take();
            System.out.println(item);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
