package ru.ifmo.ctddev.igushkin.producer;

/**
 * Created by Sergey.
 */
public class Producer<X, Y> {
    private final TaskFactory<X, Y>    factory;
    private final ArgumentsProvider<Y> argumentsProvider;
    private final Worker<X, Y> worker;

    public Producer(TaskFactory<X, Y> factory, ArgumentsProvider<Y> argumentsProvider, Worker<X, Y> worker) {
        this.factory = factory;
        this.argumentsProvider = argumentsProvider;
        this.worker = worker;
    }

    public void produceTask() {
        worker.addTask(factory.makeTask(), argumentsProvider.makeArgument());
    }
}
